//
//  ViewController.swift
//  SoundQuizInitialPrototype
//
//  Created by apple on 12/03/2015.
//  Copyright (c) 2015 Aaron Baker. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, CorrectAnswerViewControllerDelegate {
    
    var questionService: QuestionService!
    var currentQuestion = 0
    var success = false
    
    let defaults = NSUserDefaults.standardUserDefaults();
    
    var questions: [Question]!
    
    var audioPlayer = AVAudioPlayer()
    
    var sound1 = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("sounds/LIONHEART", ofType: "mp3")!)

    @IBAction func playSound(sender: AnyObject) {
        
        audioPlayer.play()
    }
    
    
    @IBOutlet weak var optionOneButton: UIButton!
    
    @IBOutlet weak var optionTwoButton: UIButton!
    
    @IBOutlet weak var optionThreeButton: UIButton!
    
    @IBOutlet weak var optionFourButton: UIButton!
    
    
    @IBOutlet weak var myImageView1: UIImageView!
    @IBOutlet weak var myImageView2: UIImageView!
    @IBOutlet weak var myImageView3: UIImageView!
    @IBOutlet weak var myImageView4: UIImageView!
    @IBOutlet weak var myImageView5: UIImageView!
    @IBOutlet weak var myImageView6: UIImageView!
    @IBOutlet weak var myImageView7: UIImageView!
    @IBOutlet weak var myImageView8: UIImageView!
    @IBOutlet weak var myImageView9: UIImageView!
    @IBOutlet weak var myImageView10: UIImageView!
    
    
    let image0 = UIImage(named:"dot1.png")
    let image1 = UIImage(named:"dot2.png")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionService = QuestionService()
        questions = questionService.loadQuestions()

        
        println(defaults.integerForKey("currentLevel"))
        if (0 ... questions.count - 1) ~= defaults.integerForKey("currentLevel"){
            currentQuestion = defaults.integerForKey("currentLevel")
        }
        
        let soundFilePath = "sounds/\(questions[currentQuestion].soundFilePath)" as String
        
        sound1 = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(soundFilePath, ofType: "mp3")!)
        
        
        audioPlayer = AVAudioPlayer(contentsOfURL: sound1, error: nil)
        audioPlayer.prepareToPlay()
        
       
        
        myImageView1.image=image0
        myImageView2.image=image0
        myImageView3.image=image0
        myImageView4.image=image0
        myImageView5.image=image0
        myImageView6.image=image0
        myImageView7.image=image0
        myImageView8.image=image0
        myImageView9.image=image0
        myImageView10.image=image0
        progressImageDone()
        updateText(questions[currentQuestion])
       
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func optionOne(sender: AnyObject) {
        checkSuccess(1)
    }

    @IBAction func optionTwo(sender: AnyObject) {
        checkSuccess(2)
    }
    
    @IBAction func optionThree(sender: AnyObject) {
        checkSuccess(3)
    }
    
    @IBAction func optionFour(sender: AnyObject) {
        checkSuccess(4)
    }
    
    
    func nextQuestion() {
        currentQuestion = (currentQuestion + 1) % questions.count
        updateText(questions[currentQuestion])
        progressImageDone()
    }
    
    
    func checkSuccess(chosenOption: Int){
        if chosenOption == questions[currentQuestion].correctAnswer {
            println("success")
            goToSuccessScreen()
            success = true
            defaults.setObject(currentQuestion + 1, forKey: "currentLevel")
            
            defaults.synchronize()
            
        
            
        } else {
            println("failure")
            goToFailureScreen()
        }
    }
    
    func goToSuccessScreen() {
        self.performSegueWithIdentifier("successSegue", sender: self)
    }
    
    func goToFailureScreen(){
        self.performSegueWithIdentifier("failureSegue", sender: self)
    }
    
    func progressImageDone (){
        
        if currentQuestion > 0{
            myImageView1.image=image1
        } else {myImageView1.image=image0}
        
        if currentQuestion > 1{
           myImageView2.image=image1
        } else {myImageView2.image=image0}
        
        if currentQuestion > 2{
            myImageView3.image=image1
        } else {myImageView3.image=image0}
        
        if currentQuestion > 3{
            myImageView4.image=image1
        } else {myImageView4.image=image0}
        
        if currentQuestion > 4{
            myImageView5.image=image1
        } else {myImageView5.image=image0}
        
        if currentQuestion > 5{
            myImageView6.image=image1
        } else {myImageView6.image=image0}
        
        if currentQuestion > 6{
            myImageView7.image=image1
        } else {myImageView7.image=image0}
        
        if currentQuestion > 7{
            myImageView8.image=image1
        } else {myImageView8.image=image0}
        
        if currentQuestion > 8{
            myImageView9.image=image1
        } else {myImageView9.image=image0}
        
        if currentQuestion > 9{
            myImageView10.image=image1
        } else {myImageView10.image=image0}
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "successSegue" {
            let controller = segue.destinationViewController as CorrectAnswerViewController
            
            controller.delegate = self
            controller.factText = questions[currentQuestion].fact
            controller.questionCount = questions.count
            controller.currentQuestion = currentQuestion
            
        }
    }
    
    
    func CorrectAnswerViewControllerNext(controller: CorrectAnswerViewController) {
       
        if currentQuestion >= questions.count - 1 {
            dismissViewControllerAnimated(true, completion: {
                self.performSegueWithIdentifier("victorySegue", sender: self)
            })
            
        } else {
            dismissViewControllerAnimated(true, completion: nil)
                self.nextQuestion()
        }
    }
    
    func CorrectAnswerViewControllerCancel(controller: CorrectAnswerViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func updateText(question: Question){
        optionOneButton.setTitle(question.options[0], forState: .Normal)
        optionTwoButton.setTitle(question.options[1], forState: .Normal)
        optionThreeButton.setTitle(question.options[2], forState: .Normal)
        optionFourButton.setTitle(question.options[3], forState: .Normal)
        
        let soundFilePath = "sounds/\(questions[currentQuestion].soundFilePath)" as String
        
        sound1 = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(soundFilePath, ofType: "mp3")!)
        audioPlayer = AVAudioPlayer(contentsOfURL: sound1, error: nil)
        audioPlayer.prepareToPlay()
    }
    
}

