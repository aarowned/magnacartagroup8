//
//  CorrectAnswerViewController.swift
//  SoundQuizInitialPrototype
//
//  Created by apple on 13/03/2015.
//  Copyright (c) 2015 Aaron Baker. All rights reserved.
//

protocol CorrectAnswerViewControllerDelegate: class {
    func CorrectAnswerViewControllerCancel(controller: CorrectAnswerViewController)
    func CorrectAnswerViewControllerNext(controller: CorrectAnswerViewController)
}

import Foundation
import UIKit




class CorrectAnswerViewController: UIViewController {
    
    var factText: String!
    var currentQuestion: Int!
    var questionCount: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        factTextField.text = factText
    }
    
    @IBOutlet weak var factTextField: UITextView!
    
    
    weak var delegate:CorrectAnswerViewControllerDelegate?

    
    @IBAction func nextQuestion(sender: AnyObject) {
        delegate?.CorrectAnswerViewControllerNext(self)
    }
    
}