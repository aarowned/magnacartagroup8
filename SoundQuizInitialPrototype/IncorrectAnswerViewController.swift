//
//  IncorrectAnswerViewController.swift
//  SoundQuizInitialPrototype
//
//  Created by Aaron Baker on 30/04/2015.
//  Copyright (c) 2015 Aaron Baker. All rights reserved.
//

import Foundation
import UIKit


class IncorrectAnswerViewController: UIViewController {
    
    override func viewDidLoad(){
        self.view.backgroundColor = UIColor(white: 0, alpha: 0.4)
        
        self.view.viewWithTag(10)!.layer.cornerRadius = 8.0
        self.view.viewWithTag(10)!.clipsToBounds = true
        
        
    }
   
    @IBAction func tryAgainButton(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}