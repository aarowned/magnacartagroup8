import Foundation

class Question {
    var soundFilePath: String
    var options: [String]
    var correctAnswer: Int
    var fact: String

    init(soundFilePath: String, options: [String], correctAnswer: Int, fact: String) {
        self.soundFilePath = soundFilePath
        self.options = options
        self.correctAnswer = correctAnswer
        self.fact = fact
    }
}