import Foundation

class QuestionService {
    
    init(){
    }
    
    func loadQuestions() -> [Question] {
        let questionsJson = jsonResponse()
        let questionsJsonArray = questionsJson["questions"]! as [[String: AnyObject]]
        
        var questionsArray = [Question]()
        
        for questions in questionsJsonArray {
            let soundFile = questions["soundfile"] as String
            let option1 = questions["option1"] as String
            let option2 = questions["option2"] as String
            let option3 = questions["option3"] as String
            let option4 = questions["option4"] as String
            let answer = questions["answer"] as Int
            let qFact = questions["fact"] as String
            
            let question = Question(soundFilePath: soundFile, options: {
                return [option1, option2, option3, option4]
                }(), correctAnswer: answer, fact: qFact)
            questionsArray.append(question)
            
        }
        return questionsArray
    }
    
    func jsonResponse() -> [String : AnyObject] {
        let path = NSBundle.mainBundle().pathForResource("questions", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        let json: AnyObject! = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil)
        return json as [String : AnyObject]
    }
}
